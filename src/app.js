const express = require("express");
require("./db/mongoose");
const app = express();
const i18next = require("i18next");
const Backend = require("i18next-fs-backend");
const middleware = require('i18next-http-middleware');

i18next.use(Backend).use(middleware.LanguageDetector)
    .init({
        fallbackLng: "en",
        backend: {
            loadPath: './locales/{{lng}}/translations.json'
        }
    })

app.use(middleware.handle(i18next))
app.use(express.json())
app.use("/users", require('./routers/user'))
app.use("/tasks", require("./routers/task"))

module.exports = app
