const express = require('express');
const router = new express.Router()
const { createUser, loginUser, logoutUser, logoutAll, getMe, updateUser, deleteUser } = require("../controllers/userController")
const User = require('../models/user');
const auth = require("../middleware/auth")
const multer = require("multer");
const sharp = require("sharp")

// Create User
router.post("/", createUser)

// Auth for Login
router.post("/login", loginUser)

// logout
router.post("/logout", auth, logoutUser)

// logoutAll
router.post("/logoutAll", auth, logoutAll)

// Read User
router.get("/me", auth, getMe)

// Update User by id
router.patch("/me", auth, updateUser)

// Delete User by Id
router.delete("/me", auth, deleteUser)

const upload = multer({
    limits: {
        fileSize: 1000000
    },
    fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
            return cb(new Error("Please upload an image"))
        }
        cb(undefined, true)
    }
})

router.post("/me/avatar", auth, upload.single('avatar'), async (req, res) => {
    const buffer = await sharp(req.file.buffer).resize({ width: 250, height: 250 }).png().toBuffer();
    req.user.avatar = buffer
    // req.user.avatar = req.file.buffer;
    await req.user.save()
    res.send()
}, (error, req, res, next) => {
    res.status(400).send({ error: error.message })
})

router.delete("/me/avatar", auth, async (req, res) => {
    req.user.avatar = undefined;
    await req.user.save();
    res.send()
})

router.get("/:id/avatar", async (req, res) => {
    try {
        const user = await User.findById(req.params.id);
        if (!user || !user.avatar) {
            throw new Error()
        }
        res.set('Content-type', 'image/png');
        res.send(user.avatar)
    } catch (error) {
        res.status(404).send()
    }
})

module.exports = router;